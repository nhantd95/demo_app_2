Rails.application.routes.draw do
  root "static_pages#home"
  devise_for :users
    as :user do
   	get "/signup" => "devise/registrations#new"
   	post "signup" => "devise/registrations#create"
    get "/login" => "devise/sessions#new"
    post "/login" => "devise/sessions#create"
    delete "/logout" => "devise/sessions#destroy"
  end
  resources :users
  resources :posts
  mount Commontator::Engine => '/commontator'
end