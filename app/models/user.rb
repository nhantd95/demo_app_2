class User < ApplicationRecord

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :confirmable, :validatable
  validates :username, presence: true, uniqueness: true
  has_many :posts, dependent: :destroy
  acts_as_commontator
  def self.find_for_database_authentication(conditions={})
  	find_by(username: conditions[:email]) || find_by(email: conditions[:email])
	end
end
