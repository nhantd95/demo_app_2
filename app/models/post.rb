class Post < ApplicationRecord
  belongs_to :user
  acts_as_commontable dependent: :destroy
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader	
  validates :user_id, presence: true
  validates :content, presence: true, length: {maximum: 250}
  validates :title, presence: true, length: {maximum: 150}
end
