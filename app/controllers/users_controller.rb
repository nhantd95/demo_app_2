class UsersController < ApplicationController
	before_action :authenticate_user!
  before_action :correct_user, only: [:show, :edit, :destroy]

  def index
    @users = User.all
  end

	def show
    @user = User.find(params[:id])
    @posts = @user.posts
  end

  def destroy
	  User.find(params[:id]).destroy
	  redirect_to root_url
  end

  def edit
  	@user = User.find(params[:id])
  end

  def update
	  @user = User.find(params[:id])
	  	if @user.update_attributes(user_params)
	      redirect_to root_url
	    else
	      render 'edit'
	    end
  end

  private

  def user_params
  	params.require(:user).permit(:username, :email, :password, :password_confirmation)
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user == @user
  end
end

