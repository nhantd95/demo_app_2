class PostsController < ApplicationController
	before_action :authenticate_user!
	before_action :correct_user, only:[:edit, :update, :destroy]
	def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
    	flash[:danger] = "Failed!"
    end
  end

  def destroy
  	@post = Post.find(params[:id])
  	if @post.destroy
  		redirect_to root_url
  		flash[:success] = "Post deleted!"
  	end
  end

  def edit
  	@post = Post.find(params[:id])
  end

  def update
  	@post = Post.find(params[:id])
  	if @post.update_attributes(post_params)
  		flash[:success] = "Edited successful!"
  		redirect_to root_url
  	else
  		render 'edit'
  	end
  end

  private

  def post_params
    params.require(:post).permit(:content, :title, :picture)
  end

  def correct_user
    @post = current_user.posts.find_by(id: params[:id])
    redirect_to(root_url) unless current_user == @post.user
  end
end
