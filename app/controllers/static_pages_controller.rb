class StaticPagesController < ApplicationController
	def home
		@users = User.all
		@post = current_user.posts.build if signed_in?
	end
end
